# Desktop

Projekty dekstopowe, w tym na gogle Oculus Rift oraz HTC Vive/Cosmos

## Spis projektów

 - [Wdrożenie dla Velvet Care](#wdrożenie-dla-velvet-care)
 - [Interakcja Rękawiczki Hi5](#interakcja-rękawiczki-hi5)
 - [Pożar w samolocie](#pożar-w-samolocie)
 - [Operator Suwnicy](#operator-suwnicy)
 - [Wirtualna Galeria Dyplomów](#wirtualna-galeria-dyplomów)
 - [Polska Siła Obrazu - Wirtualny spacer](#polska-siła-obrazu)
 - [Konfigurator nagrobka](#konfigurator-nagrobka)
 - [Minebest](#minebest)
 - [Medical Training](#medical-training)
 - [Szkolenia VR dla produkcji](#szkolenia-vr-dla-produkcji)
 - [Funny VR](#funny-vr)

#

### Wdrożenie dla Velvet Care


Projekt VR dla firmy Velvet Care polegający na wdrożeniu w świat wirtualny aplikacji szkoleniowej dla przyszłych pracowników firmy. Projekt zrealizowaliśmy dzięki akceleracji Krakowskiego Parku Technologicznego w ramach programu KPT ScaleUp, współfinansowanego przez PARP przy wsparciu środków UE. Poniżej dwa filmy prezentujące projekt:

<a align='center' href="https://youtu.be/6oB826HRCJg" target="_blank"><img src="http://img.youtube.com/vi/6oB826HRCJg/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/09LeaiCqLQ4" target="_blank"><img src="http://img.youtube.com/vi/09LeaiCqLQ4/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

Projekt realizowany na silniku graficznym Unity 2019.x, z wykorzystaniem takich narzędzi, jak: [HTC Vive Cosmos](https://www.vive.com/eu/product/vive-cosmos/features/), [Hi5](https://hi5vrglove.com/), [Capto Gloves](https://www.captoglove.com/)

#

### Interakcja Rękawiczki Hi5


Własny system teleportacji przy pierwszej styczności z technologią [rękawiczek Hi5](https://hi5vrglove.com/) w połączeniu z HTC Vive/Cosmos. Bardziej wtejemnczonym: wzory i dźwięki elementów do teleportacji wykorzystane z assetu [SteamVR Plugin](https://assetstore.unity.com/packages/tools/integration/steamvr-plugin-32647) (tylko grafiki i dźwięki).
Teleportacja wykonywana za pomocą gestów wskazania palcem wskazującym, a następnie otwarcia ręki. W pierwszej połowie filmu miałem problemy z trackowaniem ręki. Po restarcie aplikacji (druga połowa filmu) - zaczyna to jakoś działać ;)
Dodatkowo inne interakcje kompatybilne z rękawiczkami hi5.


<a align='center' href="https://youtu.be/9Nq2s6odS5w" target="_blank"><img src="http://img.youtube.com/vi/9Nq2s6odS5w/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/L6z1lYoyjL4?t=31" target="_blank"><img src="http://img.youtube.com/vi/L6z1lYoyjL4/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>
 
#


### Pożar w samolocie

Projekt VR na gogle [HTC Vive]() realizowany w silniku graficznym Unity 2019.x, więcej na temat projektu:

<a align='center' href="https://youtu.be/lAGuI8L71Og" target="_blank"><img src="http://img.youtube.com/vi/lAGuI8L71Og/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#


### Operator suwnicy

Aplikacja, w której siadamy za sterami suwnicy, w którem uwzględniono ruch dźwigniami, fizykę liny, podnoszenię obiektów (z uwzględnieniem wagi), opuszczanie obiektów.
Myślę, że szerszy opis pozostawiam w odnośniku do strony: [EpicVR.pl](https://epicvr.pl/pl/suwnica-vr-realistyczny-symulator-szkoleniowy-dla-operatorow-suwnic-w-wirtualnej-rzeczywistosci/).
Aplikacja przeznaczona na gogle HTC Vive.


<a align='center' href="https://youtu.be/H54t0SxK7f4" target="_blank"><img src="http://img.youtube.com/vi/H54t0SxK7f4/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Wirtualna Galeria Dyplomów

Aplikacja stricte desktopowa, z wykorzystaniem [HDRP](https://blogs.unity3d.com/2018/09/24/the-high-definition-render-pipeline-getting-started-guide-for-artists/). Niestety, ze względu na ograniczenia sprzętowe klienta, musieliśmy zrezygnować z wielu funckjonalności, jakie oferuje ten renderpipeline - a szkoda.

<a align='center' href="https://youtu.be/k26D7wwdlW0" target="_blank"><img src="http://img.youtube.com/vi/k26D7wwdlW0/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


#


### Polska Siła Obrazu

Projekt realizowany dla MNW w Unity 2019.x. Tym razem trzeba było zmierzyć się z dużymi plikami dźwiękowymi jak i filmami video, złożonymi modelami niektórych ram, a także w miarę możliwości ładnie przedstawić otoczenie projektu w aplikacji WebGL, gdzie dość duże znaczenie stawiano na płynność oraz szybkość ładowania. Wykorzystane zostały tutaj przeróżne sztuczki i szamańskie tańce, aby dopiąć projekt na ostatni guzik :). Link do projektu: [Polska Siła Obrazu MNW](http://polskasilaobrazu.mnw.art.pl/3d/index.html?lang=PL).

#

### Konfigurator nagrobka

Realizowany dla firmy Lastryco projekt umożliwiający konfiugrację nagrobka. Projekt był realizowany na silniku graficznym Unity 2019.x i docelowo projekt był obsługiwany przez ekran dodtykowy, nagranie poniżej:


<a align='center' href="https://youtu.be/gaUqYWmm4xo" target="_blank"><img src="http://img.youtube.com/vi/gaUqYWmm4xo/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


#

### Minebest

Aplikacja VR przeznaczona na gogle HTC Vive. Aplikacja sama w sobie jest dużo bardziej rozbudowana, ale ze zględu na poufność informacji: nie można ich udostępnić.
Sama aplikacja oferuje użytkownikowi wirtualny świat, w którym budujemy własną farmę BTC. Do dyspozycji dostajemy różnego rodzaju narzędzia i przy wsparciu Robodrona idziemy krok po kroku, w celu uruchomienia kolejnej koparki.

<a align='center' href="https://youtu.be/y17CrXJ5f38" target="_blank"><img src="http://img.youtube.com/vi/y17CrXJ5f38/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


**Nagrania z testów**

<i>Odkręcanie śrubek:</i><br>
<a align='center' href="https://youtu.be/dY8jCjjVitU" target="_blank"><img src="http://img.youtube.com/vi/dY8jCjjVitU/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


<i>Otwieranie drzwi: Uwaga, w ~39s na nagraniu włącza się nieprzyjemny szum ;) - problem z mikrofonem</i><br>
<a align='center' href="https://youtu.be/Won6MoqY710" target="_blank"><img src="http://img.youtube.com/vi/Won6MoqY710/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


<i>Menu kołowe:</i><br>
<a align='center' href="https://youtu.be/U9D5pD9g6fM" target="_blank"><img src="http://img.youtube.com/vi/U9D5pD9g6fM/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<i>Nauka latania:</i><br>
<a align='center' href="https://youtu.be/fP652o0Zo2c" target="_blank"><img src="http://img.youtube.com/vi/fP652o0Zo2c/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>
<a align='center' href="https://youtu.be/baCU_5RPA1c" target="_blank"><img src="http://img.youtube.com/vi/baCU_5RPA1c/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<i>Pierwsze testy dla systemu zadań:</i><br>
<a align='center' href="https://youtu.be/_UGzvrBDqiQ" target="_blank"><img src="http://img.youtube.com/vi/_UGzvrBDqiQ/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>
<a align='center' href="https://youtu.be/Mu2CjN74HxE" target="_blank"><img src="http://img.youtube.com/vi/Mu2CjN74HxE/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Medical Training

Aplikacja VR na HTC Vive. W aplikacji użytkownik ma możliwość poznania anatomii człowieka. Poniższe materiały przedstawiają kilka wersji aplikacji na różnych etapach pracy.

<a align='center' href="https://youtu.be/_LQZrpxzGsg" target="_blank"><img src="http://img.youtube.com/vi/_LQZrpxzGsg/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/i_g12LnNg0s" target="_blank"><img src="http://img.youtube.com/vi/i_g12LnNg0s/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/0pNk6DilIrk" target="_blank"><img src="http://img.youtube.com/vi/0pNk6DilIrk/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/6E8ZhCsGrkI" target="_blank"><img src="http://img.youtube.com/vi/6E8ZhCsGrkI/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Szkolenia VR dla produkcji

Aplikacja VR przeznaczona na gogle HTC Vive. 


<a align='center' href="https://youtu.be/rSOCElV0jOc" target="_blank"><img src="http://img.youtube.com/vi/rSOCElV0jOc/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### Funny VR

Aplikacvja VR na gogle HTC Vive. Robiona w formie zapoznania się z goglami.

<a align='center' href="https://youtu.be/LHdf5wGCBAc" target="_blank"><img src="http://img.youtube.com/vi/LHdf5wGCBAc/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/tk8ruJAaHeA" target="_blank"><img src="http://img.youtube.com/vi/tk8ruJAaHeA/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/hmtrcXX4IRs" target="_blank"><img src="http://img.youtube.com/vi/hmtrcXX4IRs/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>


